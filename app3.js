import fs from "fs";
import http from "http";

const server = new http.Server();

server.on("request", (req, res) => {
  if (req.url === "/favicon.ico") {
    res.end("");
    return;
  }
  const file = fs.createReadStream("./package-lock.json");

  // file.on("data", (chunk) => {
  //   const canProceed = res.write(chunk);
  //   if (canProceed) {
  //     return;
  //   }

  //   file.pause();

  //   res.once("drain", () => {
  //     file.resume();
  //   });
  // });

  // file.on("end", () => {
  //   res.end();
  // });

  file.on("error", () => {
    res.statusCode = 404;
    res.end("");
  });

  // file.pipe(res);
  // file.on('data', (chunk) => console.log('data'));
  // file.resume();
  file.on('end', () => console.log('end'));

  res.on("close", () => file.destroy());
});

server.on("error", () => {});

server.listen(3000);
