import EventEmitter from 'events';

console.clear();

const emitter = new EventEmitter();

emitter.on('hello', (...arg) => {
  console.log('Hello1', arg);
});

emitter.once('hello', (...arg) => {
  console.log('Hello2', arg);
});

emitter.prependListener('hello', (...arg) => {
  console.log('Hello0', arg);
});

emitter.emit('hello', 1, 1, 3, 2);
emitter.emit('hello', 1222, 444, 345);

console.log('after');

console.log(emitter.eventNames());
emitter.removeAllListeners('hello');
console.log(emitter.listeners('hello'));
