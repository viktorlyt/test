import http from "http";

const server = new http.Server();

const data = {
  x: 1,
  y: 2,
};

server.on("request", (req, res) => {
  // res.write("Hi ");
  const chunks = [];
  req.on("data", (chunk) => {
    chunks.push(chunk);
  });
  req.on("end", () => {
    const text = Buffer.concat(chunks).toString();
    const data = JSON.parse(text);
    console.log(text, data);
  });
  res.write(JSON.stringify(data));
  // res.end("you!!!!!!!!!!!!!\n");
  res.end("");
});

server.on("error", () => {});

server.listen(3000);
