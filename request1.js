import http from "http";

const url = "http://localhost:3000";
const options = {
  method: "POST",
};

const data = {
  x: 17,
  y: 27,
};

const req = http.request(url, options, (res) => {
  // res.pipe(process.stdout);
  const chunks = [];
  res.on("data", (chunk) => {
    chunks.push(chunk);
  });

  res.on("end", () => {
    const text = Buffer.concat(chunks).toString();
    const data = JSON.parse(text);
    console.log(text, data);
  });
});

req.end(JSON.stringify(data));
