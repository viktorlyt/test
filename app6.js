import http from "http";

const server = new http.Server();

const data = {
  x: 1,
  y: 2,
};

server.on("request", async (req, res) => {
  const chunks = [];

  for await (const chunk of req) {
    chunks.push(chunk);
  }
  const text = Buffer.concat(chunks).toString();
  const data = JSON.parse(text);
  console.log(text, data);

  res.write(JSON.stringify(data));
  res.end("");
});

server.on("error", () => {});

server.listen(3000);
