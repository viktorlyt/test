import http from 'http';

console.clear();

const PORT = process.env.PORT || 3000;
const server = new http.Server();

server.on('request', (req, res) => {
  if (req.url === '/error') {
    server.emit('error', new Error());
  }

  res.end('Done');
});

server.on('error', (err) => {
  console.log('Error occured', err);
})

const emit = server.emit;

server.emit = (eventName, ...args) => {
  console.log(eventName);
  emit.call(server, eventName, ...args);
};

server.listen(PORT, () => {
  console.log(`Server listening on port http://localhost:${PORT}`);
});