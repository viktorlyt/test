import fs from "fs";
// import fs1 from 'fs/promises';

// try {
//   const data = fs.readFile('./1.txt');
//   console.log(data);
// } catch (error) {
//   console.log(error);
// }

// fs1.readFile('./1.txt', 'utf8')
//   .then(data => {console.log(data)})
//   .catch(error => {console.log(error)});
import http from "http";
import { Readable, Writable } from "stream";

const server = new http.Server();

process.stdin.on("data", (chunk) => {
  setTimeout(() => {
    process.stdout.write("> " + chunk);
  }, 1000);
});

server.on("request", (req, res) => {
  // new Readable();
  // fs.createReadStream('./package.json');
  // process.stdin
  // req

  // new Writable();
  // fs.createWriteStream('./copy');
  // process.stdout
  // process.stderr
  // res

  // if (req.url === "/download") {
  //   const fileStream = fs.createReadStream("./song.mp3");

  //   res.setHeader("Content-Type", "audio/mpeg");
  //   res.setHeader("Content-Disposition", "attachment; filename=song.mp3");

  //   fileStream.pipe(res);

  //   fileStream.on("end", () => {
  //     console.log(process.memoryUsage().external);
  //     console.log("File was read successfully");
  //   });

  //   fileStream.on("error", (err) => {
  //     res.statusCode = 500;
  //     res.end("Error downloading");
  //   });

  // fs.readFile('./Event-Emitter.mp4', (err, data) => {
  //   if (err) {
  //     res.statusCode = 500;
  //     res.end('Error downloading');
  //     return;
  //   }
  //   res.setHeader('Content-Type', 'video/mp4');
  //   res.setHeader('Content-Disposition', 'attachment; filename=Event-Emitter.mp4');
  //   res.end(data);
  // });
  // return;

  res.setHeader("Content-Type", "text/html");

  for (let i = 5; i > 0; i--) {
    setTimeout(() => {
      res.write(`<p>${i}</p>`);
    }, (5 - i) * 1000);
  }

  setTimeout(() => {
    res.end(`<p>Done!</p>`);
  }, 5000);

  // } else {
  //   res.setHeader("Content-Type", "text/html");
  //   res.end(`
  //     <a href="/download" target="_blank">
  //       Download a file
  //     </a>
  //   `);
  // }
});

server.on("error", () => {});

server.listen(3000);
