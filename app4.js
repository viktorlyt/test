import fs from "fs";
import http from "http";
import path from "path";
import { pipeline } from "stream";
import zlib from "zlib";

const server = new http.Server();

server.on("request", (req, res) => {
  const url = new URL(req.url, `http://${req.headers.host}`);
  const fileName = url.pathname.slice(1) || "index.html";
  const filePath = path.resolve("public", fileName);

  if (!fs.existsSync(filePath)) {
    res.statusCode = 404;
    res.end("File not found");
    return;
  }

  res.setHeader("Content-Encoding", "gzip");
  // res.setHeader("Content-Encoding", "br");

  const file = fs.createReadStream(filePath);

  const gzip = zlib.createGzip();
  // const gzip = zlib.createBrotliCompress();

  file
    .on("error", (error) => {})
    .pipe(gzip)
    .on("error", (error) => {})
    .pipe(res)
    .on("error", (error) => {});

  // pipeline(file, gzip, res, (error) => {});
  gzip.pipe(fs.createWriteStream(filePath + ".gzip1"));

  file.on("error", () => {
    res.statusCode = 500;
    res.end("File error");
  });

  res.on("close", () => {
    file.destroy();
  });
});

server.on("error", () => {});

server.listen(3000);
