import http from 'http';
import axios from 'axios';

const BASE = 'http://localhost:8080';
const pathname = '/users/3/friends';
const search = '?sex=m&age=25&age=45'
const href = BASE + pathname + search;

console.log(href);

axios.get(href)
  .then(res => {
    console.log(res.status);

    console.log(res.data);
  })
  .catch(err => {
    console.error(err);
  })

// const req = http.request('http://localhost123:8080', (res) => {
//   console.log(res.statusCode);

//   res.setEncoding('utf8');

//   res.on('data', (data) => {
//     console.log(data);
//   });
// });

// req.on('error', (err) => {
//   console.error(err);
// });

// req.end();